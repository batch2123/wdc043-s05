package com.zuitt.batch212;

public class Contact {

    private String name;
    private String numbers;
    private String addresses;

    public Contact() {
        super();
    }

    public Contact(String name, String numbers, String addresses) {
        this.name = name;
        this.numbers = numbers;
        this.addresses = addresses;
    }



    //    Setters
    public void setName(String name){this.name = name;}

    public void setNumbers(String numbers) {
        this.numbers = numbers;
    }

    public void setAddresses(String addresses){this.addresses = addresses;}

//    Getters

    public String getName(){return  this.name;}

    public String getNumbers(){return this.numbers;}


    public String getAddresses(){return this.addresses;}

    public void call(){System.out.println(getName());}



}
