package com.zuitt.batch212;

import java.util.HashMap;

public class Main {

    public static void main(String[] args) {

        Contact contact1 = new Contact();

        contact1.setName("Greg");
        System.out.println(contact1.getName());
        System.out.println("******************");

        System.out.println(contact1.getName() + " " + "Has the following registered numbers:");
        contact1.setNumbers("+1273981238123 \n" + "+1203810923912");
        System.out.println(contact1.getNumbers());
        System.out.println("---------------------------------");


        System.out.println(contact1.getName() + " " + "Has the following registered addresses:");
        contact1.setAddresses("Manila \n" + "Bulacan");
        System.out.println(contact1.getAddresses());
        System.out.println("=============================");










    }
}
